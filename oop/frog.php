<?php
require_once('animal.php');

class frog extends Animal{
    public $legs = 4;
    public $cold_blooded = "no";
    public function jump(){
        echo "Jump : Hop-hop";
    }
}
?>