<?php
require_once('animal.php');
require_once('kera.php');
require_once('frog.php');
$sheep = new Animal("shaun");
echo "Nama hewan: " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah kaki: " . $sheep->legs . "<br>"; // 4
echo "Cold Blood: ".$sheep->cold_blooded; // "no"

echo "<br><br>";

$kera = new kera("Kera");
echo "Name : " . $kera->name . "<br>";
echo "legs : " . $kera->legs . "<br>";
echo "cold blooded :" . $kera->cold_blooded;
echo "<br>";
echo  $kera->yell();

echo "<br><br>";

$katak = new frog("Katak");
echo "Name : " . $katak->name . "<br>";
echo "legs : " . $katak->legs . "<br>";
echo "cold blooded :" . $katak->cold_blooded;
echo "<br>";
echo  $katak->jump();
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>